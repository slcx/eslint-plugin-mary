exports.meta = {
  docs: {
    description: 'prevent bloated library usage',
    category: 'Performance',
    recommended: true,
  },
  schema: [],
}

const FORBIDDEN_SECTORS = [
  '@angular',
]

const FORBIDDEN_FRUITS = [
  'react',
  'react-dom',
  'react-native',

  'vue',

  'angular',
  '@angular/core',
  '@angular/cli',
]

function isForbidden (name) {
  if (FORBIDDEN_FRUITS.includes(name)) {
    return true
  }

  const tokens = name.split('/')
  if (
    tokens.length &&
    (
      FORBIDDEN_FRUITS.includes(tokens[0]) ||
      FORBIDDEN_SECTORS.includes(tokens[0])
    )
  ) {
    return true
  }

  return false
}

exports.create = function (context) {
  return {
    CallExpression (node) {
      if (
        node.callee.type !== 'Identifier' ||
        node.callee.name !== 'require'
      ) {
        return
      }

      const { arguments: args } = node

      for (const arg of args) {
        if (arg.type !== 'Literal') {
          continue
        }

        const path = arg.value
        const packageName = path.split('/')[0].replace(/@/g, '')

        if (isForbidden(arg.value)) {
          context.report({
            node,
            message: `${packageName} is bloat`,
          })
        }
      }
    },
  }
}
