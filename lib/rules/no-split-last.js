exports.meta = {
  docs: {
    description: 'disallow .split usage to find the last token',
    category: 'Performance',
    recommended: true,
  },
  schema: [],
}

/* eslint-disable-next-line complexity */
function checkArrayAccess (context, node) {
  // only target []
  if (!node.computed) {
    return
  }

  // the object being accessed isn't a var!
  if (node.object.type !== 'Identifier') {
    return
  }

  const property = node.property

  // bail if the expr inside of [] isn't a binary expression
  // (we're looking for thing.length - 1)
  if (property.type !== 'BinaryExpression') {
    return
  }

  const { left, right, operator } = property

  if (operator !== '-') {
    return
  }

  // validate left side: must be a .length member access
  if (!(
    left.type === 'MemberExpression' &&
    left.property.name === 'length' &&
    !left.computed
  )) {
    return
  }

  // validate right side: must be a literal
  if (!(
    right.type === 'Literal' &&
    right.value === 1
  )) {
    return
  }

  const arrayName = node.object.name // the array of the .split results
  const scope = context.getScope()
  const array = scope.variables.find((variable) => variable.name === arrayName)

  // hmm, not in scope.
  if (!array) {
    return
  }

  // validate: must be defined in scope
  if (array.defs.length === 0) {
    return
  }

  const definition = array.defs[0]
  const defNode = definition.node
  const { init } = defNode

  // validate: was initialized with a call?
  if (init.type !== 'CallExpression') {
    return
  }

  const { callee } = init

  // validate: was called with split?
  if (callee.property.name !== 'split') {
    return
  }

  context.report({
    node,
    message: 'lastIndexOf and substring should be used for enhanced performance',
  })
}

exports.create = function (context) {
  return {
    MemberExpression: (node) => checkArrayAccess(context, node),
  }
}
