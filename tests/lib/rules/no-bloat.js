const rule = require('../../../lib/rules/no-bloat')
const { RuleTester } = require('eslint')

const tester = new RuleTester()

tester.run('no-bloat', rule, {
  invalid: [
    {
      code: "require('react')",
      errors: [{ message: 'react is bloat' }],
    },
    {
      code: "require('react/lib/index')",
      errors: [{ message: 'react is bloat' }],
    },
    {
      code: "require('@angular/core')",
      errors: [{ message: 'angular is bloat' }],
    },
    {
      code: "require('angular/special/backdoor')",
      errors: [{ message: 'angular is bloat' }],
    },
    {
      code: "require('vue')",
      errors: [{ message: 'vue is bloat' }],
    },
  ],
  valid: [
    { code: "require('vanilla-js')" },
    { code: "require('document-getelementbyid')" },
  ],
})
