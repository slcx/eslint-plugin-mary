## `mary/no-bloat`

`require`-ing bloat? Not on Mary's watch!

```js
require("react"); // ✗ avoid
require("vue"); // ✗ avoid
require("@angular/core"); // ✗ avoid

document.getElementById("element"); // ✓ ok
```

Disable this rule and bad things will happen, guaranteed!

## `mary/no-split-last`

When calling `.split` on a string to get its last token, you should
probably just use `.substring` and `.lastIndexOf`. It's faster!

```js
// ✗ avoid
let things = string.split(" ");
let last = things[things.length - 1];

// ✓ ok
let last = string.substring(string.lastIndexOf(" ") + 1);
```
